import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private authServcie: AuthService) {}

  ngOnInit(): void {}

  logout(): void {
    this.authServcie
      .logout(`${window.location.origin}/`)
      .then(() => {
        console.log('logout function called');
      })
      .catch((error) => {
        console.error('Error occurred at logout function');
      });
  }
}
