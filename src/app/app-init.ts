import { KeycloakService, KeycloakOptions } from 'keycloak-angular';
import { environment } from 'src/environments/environment';

export function initializer(keycloak: KeycloakService): ()=> Promise<any> {
  const options: KeycloakOptions = {
    bearerExcludedUrls: [],
    loadUserProfileAtStartUp: false,
    config: environment.keycloakconfig,
    initOptions: {
      flow: 'standard',
      checkLoginIframe: false,
      enableLogging: true,
      redirectUri: `${window.location.origin}/`,
      onLoad: 'check-sso',
    }
  };
  return (): Promise<any>=> keycloak.init(options);
}
