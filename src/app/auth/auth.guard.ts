import { Injectable } from '@angular/core';
import {
  KeycloakService,
  KeycloakOptions,
  KeycloakAuthGuard,
} from 'keycloak-angular';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard extends KeycloakAuthGuard {
  constructor(
    protected router: Router,
    protected keycloakAngular: KeycloakService
  ) {
    super(router, keycloakAngular);
  }

  isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    let isAuth: boolean | undefined = false;
    const instance = this.keycloakAngular.getKeycloakInstance();
    if (instance) {
      isAuth = instance.authenticated;
      if (!isAuth) {
        this.keycloakAngular.login({
          redirectUri: `${window.location.origin}/home`,
        });
        return Promise.resolve(false);
      }
      console.log('user is successfully authenticated');
      return Promise.resolve(isAuth);
    }
    return Promise.resolve(false);
  }
}
