import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakLoginOptions } from 'keycloak-js';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private KeycloakService: KeycloakService) {}

  public login(options: KeycloakLoginOptions): Promise<void> {
    return this.KeycloakService.login(options);
  }

  public getAccessToken(): Promise<string> {
    return this.KeycloakService.getToken();
  }

  public isAuthenticated(): boolean | undefined {
    return this.KeycloakService.getKeycloakInstance().authenticated;
  }

  public logout(redirectUri: string): Promise<void> {
    this.KeycloakService.clearToken();
    return this.KeycloakService.logout(redirectUri);
  }
}
