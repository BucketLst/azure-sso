import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(private authServcie: AuthService) {}

  ngOnInit(): void {}

  login(): void {
    this.authServcie
      .login({
        redirectUri: `${window.location.origin}/home`,
      })
      .then(() => {
        console.log('login function called');
      })
      .catch((error) => {
        console.error('Error occurred at login function');
      });
  }
}
